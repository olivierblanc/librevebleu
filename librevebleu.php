<?php
/**
 * @package    librevebleu
 *
 * @author     olivier <commission.internet@revebleu.org>
 * @copyright  A copyright
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 * @link       https://www.revebleu.org
 */

defined('_JEXEC') or die;

/*
$path = JPATH_LIBRARIES . '/librevebleu' ;

$componentFiles = getComponentFiles($path) ;

JLoader::registerNamespace('libphonenumber', JPATH_LIBRARIES . '/librevebleu/libphonenumber', false, false, 'psr4');
JLoader::registerNamespace('NextcloudApiWrapper', JPATH_LIBRARIES . '/librevebleu/NextcloudApiWrapper', false, false, 'psr4');
JLoader::registerNamespace('GuzzleHttp\\Promise', JPATH_LIBRARIES . '/librevebleu/guzzlehttp/promises', false, false, 'psr4');
JLoader::registerNamespace('GuzzleHttp', JPATH_LIBRARIES . '/librevebleu/guzzlehttp', false, false, 'psr4');

JLoader::registerNamespace('Psr\\Http\\Message', JPATH_LIBRARIES . '/librevebleu/psr/http-message', false, false, 'psr4');
JLoader::registerNamespace('Psr', JPATH_LIBRARIES . '/librevebleu/psr', false, false, 'psr4');

$includeFunctions = true ;
foreach (array('guzzlehttp', 'guzzlehttp/promises', 'guzzlehttp/psr7') as $aDir) {
    $includeFunctions &= require_once JPATH_LIBRARIES . '/librevebleu/' . $aDir .'/functions_include.php' ;
}

return $includeFunctions ;
*/

return require_once JPATH_LIBRARIES . '/librevebleu/vendor/autoload.php' ;